﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClosePopup : MonoBehaviour
{
    Button hideButton;
    public GameObject Panel;

    // Start is called before the first frame update
    void Start()
    {
        hideButton = GetComponent<Button>();

        hideButton.onClick.AddListener(() => Hide());
    }

    public void Hide()
    {
        Panel.gameObject.SetActive(false);
    }
}
    

