﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; //added

public class BoundaryController : MonoBehaviour
{
    //added
    private void OnTriggerEnter2D(Collider2D collision) 
    {
        GameManager.instance.incrementRestarts();
        SceneManager.LoadScene(0);
    }
}
