﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarController : MonoBehaviour
{
    public Slider healthSlider;
    public float maxHealth;
    public static float currentHealth;
    PlayerController player;

    private void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        player = FindObjectOfType<PlayerController>();
        currentHealth = player.health;
        healthSlider.value = currentHealth;
    }
}
